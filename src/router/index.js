import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layouts/index.vue'

const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '',
        name: "Home",
        component: () => import('@/pages/home/index.vue'),
      },
    ],
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/login',
        name: "Login",
        component: () => import('@/pages/login/index.vue'),
      },
    ],
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/assessmentList',
        name: "AssessmentList",
        component: () => import('@/pages/assessment/list.vue'),
      },
    ],
  },
  {
    path: '/prepare',
    component: Layout,
    children: [
      {
        path: '',
        name: "Prepare",
        component: () => import('@/pages/assessment/prepare.vue'),
      },
    ],
  },
  {
    path: '/assessing',
    component: Layout,
    children: [
      {
        path: '',
        name: "Assessing",
        component: () => import('@/pages/assessment/assessing.vue'),
      },
    ],
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/reportList',
        name: "ReportList",
        component: () => import('@/pages/report/list.vue'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/pages/login/index.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
