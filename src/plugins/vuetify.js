/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
    theme: {
        themes: {
            light: {
                colors: {
                    primary: '#23ccd1',
                    'primary-lighten-1': '#9DB0FF',
                    'primary-lighten-2': '#DEE5FF',
                    'primary-lighten-3': '#EEF1FF',
                    'primary-darken-1': '#4154CC',
                    secondary: '#F87F19',
                    accent: '#8c9eff',
                    error: '#FF5D66',
                    red: '#FF5D66',
                    success: '#53B568',
                    'grey-border': '#D9D9D9',
                    'base-background': '#F4F5F7',
                    title: '#323232',
                    context: '#404040',
                    tip: '#666666',
                    placeholder: '#999999',
                    'icon-background': '#f9fbfc',
                    'disabled': '#f6f6f6'
                },
            },
        },
    },
})
