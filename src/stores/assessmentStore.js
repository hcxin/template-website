import { defineStore } from 'pinia'

export const useAssessmentStore = defineStore('assessment', {
    state: () => {
        return {
            assessmentId: 0,
            assessmentInfo: {},
            isComplete: false,
        }
    },
})